from django.shortcuts import render


def index(request):
    context = {'sample_data': 'Hello, World!'}
    return render(request, 'module/index.html', context)
