from __future__ import absolute_import
from celery import shared_task
import logging
import module.services as services
import module.messages as messages

# Logger
logger = logging.getLogger(__name__)


@shared_task
def my_sample_task():
    """A sample task executed by Celery.
    """
    try:
        services.sample_task()
    except Exception as e:
        logger.error(messages.unexpected_error_during_sample_task(e.message))


@shared_task
def produce_to_sample_queue():
    """Produces a message to sample queue at RabbitMQ.
    """
    try:
        message = {"name": "John", "age": "43"}
        services.produce_to_sample_queue(message)
    except Exception as e:
        logger.error(messages.unexpected_error_during_produce_to_sample_queue(e.message))


@shared_task
def consume_from_sample_queue():
    """Consumes a sample queue from RabbitMQ.
    """
    try:
        services.consume_from_sample_queue()
    except Exception as e:
        logger.error(messages.unexpected_error_during_consume_from_sample_queue(e.message))
