import logging
import json
from rabbitmq.services import RabbitProducer
from rabbitmq.services import RabbitConsumer
import module.messages as messages

# Logger
logger = logging.getLogger(__name__)


def sample_task():
    """Sample task to show up celery's functionality
    """
    logger.info(messages.sample_task_done())


def produce_to_sample_queue(data):
    """Produces a message to sample queue at RabbitMQ.
    """
    RabbitProducer(
        message=json.dumps(data),
        exchange='skeleton',
        exchange_type='direct',
        routing_key='sample',
        queue='sample_queue'
    ).produce()

    logger.info(messages.sample_queue_produced(data))


def consume_from_sample_queue():
    """Consumes a sample queue from RabbitMQ.
    """
    RabbitConsumer(
        callback=consume_from_sample_queue_callback,
        exchange='skeleton',
        exchange_type='direct',
        routing_key='sample',
        queue='sample_queue'
    ).consume()


def consume_from_sample_queue_callback(ch, method, properties, body):
    """The callback definition for the sample queue.
    """
    data = json.loads(body)
    logger.info(messages.sample_queue_consumed(data))
    
    ch.basic_ack(delivery_tag=method.delivery_tag)
