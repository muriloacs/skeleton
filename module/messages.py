"""Defines the messages that will be
used throughout this module.
"""


def unexpected_error_during_sample_task(exception):
    return 'An error occurred during sample task: {0}'.format(exception)


def unexpected_error_during_produce_to_sample_queue(exception):
    return 'An error occurred during produce to sample queue: {0}'.format(exception)


def unexpected_error_during_consume_from_sample_queue(exception):
    return 'An error occurred during consume from sample queue: {0}'.format(exception)


def sample_task_done():
    return 'Sample task done!'


def sample_queue_produced(data):
    return 'Sample queue produced: {0}'.format(data)


def sample_queue_consumed(data):
    return 'Sample queue consumed: {0}'.format(data)
