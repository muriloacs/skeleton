# Skeleton

Projeto esqueleto para aplicações Django.


### Versão
1.0.0

### Instalação

Desenvolvimento:
```sh
$ apt-get install libffi-dev libssl-dev libxml2-dev libxslt1-dev
$ cd /path/to/project
$ pip install -r requirements.txt
$ ./deploy_dev.sh
```

Produção:
```sh
$ sudo yum install -y libffi libffi-devel libxml2-devel libxslt-devel
$ cd /path/to/project
$ pip install -r requirements.txt
$ ./deploy_prd.sh
```

### Tecnologias

Essas são as tecnologias utilizadas no projeto Skeleton:

* [[Django](https://www.djangoproject.com/)] - Python Framework.
* [[Celery](http://www.celeryproject.org/)] - Criação de tasks e jobs.
* [[RabbitMQ](https://www.rabbitmq.com/)] - Enfileirador de mensagens, também utilizado como broker para o Celery.
