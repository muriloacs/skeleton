import pika
from django.conf import settings


class RabbitServer:
    # Connection parameters
    host = settings.RABBIT_MQ_HOST
    port = settings.RABBIT_MQ_PORT
    user = settings.RABBIT_MQ_USER
    password = settings.RABBIT_MQ_PASSWORD

    def __init__(self, exchange, exchange_type, routing_key, queue):
        """Creates the connection with RabbitMQ server and the
        necessary stuffs to get things working (channel, queue etc).
        """
        credentials = pika.PlainCredentials(self.user, self.password)
        parameters = pika.ConnectionParameters(self.host, self.port, '/', credentials)
        self.connection = pika.BlockingConnection(parameters)
        self.channel = self.connection.channel()
        self.channel.exchange_declare(exchange=exchange, type=exchange_type)
        self.channel.queue_declare(queue=queue, durable=True)
        self.channel.queue_bind(exchange=exchange, queue=queue, routing_key=routing_key)


class RabbitProducer:
    def __init__(self, message, exchange, exchange_type, routing_key, queue):
        self.server = RabbitServer(exchange, exchange_type, routing_key, queue)
        self.message = message
        self.exchange = exchange
        self.routing_key = routing_key

    def produce(self):
        """Produces a message through an exchange and queue.
        """
        self.server.channel.basic_publish(
            exchange=self.exchange,
            routing_key=self.routing_key,
            body=self.message,
            properties=pika.BasicProperties(delivery_mode=2))  # makes message persistent
        self.server.connection.close()


class RabbitConsumer:
    def __init__(self, callback, exchange, exchange_type, routing_key, queue):
        self.server = RabbitServer(exchange, exchange_type, routing_key, queue)
        self.callback = callback
        self.queue = queue

    def consume(self):
        """Consumes messages from a queue.
        """
        self.server.channel.basic_qos(prefetch_count=1)
        self.server.channel.basic_consume(self.callback, queue=self.queue)
        self.server.channel.start_consuming()
