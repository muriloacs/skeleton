# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'database',
        'USER': 'user',
        'PASSWORD': 'password',
        'HOST': 'host',
        'PORT': '3306',
    }
}

# Configs for RabbitMQ: Responsible for getting contact info (producer), queue and send email (consumer)
RABBIT_MQ_HOST = 'localhost'
RABBIT_MQ_PORT = 5672
RABBIT_MQ_USER = 'user'
RABBIT_MQ_PASSWORD = 'password'
BROKER_URL = 'user://password:10502@localhost:5672//'