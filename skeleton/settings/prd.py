from .common import *

DEBUG = False
ALLOWED_HOSTS = ['*']
SITE_URL = 'http://www.example.com'


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/
STATIC_URL = 'http://s3.example.com/...'


""" 
    For aditional secret settings such as E-mails, Passwords and Tokens, create an addtional file. 
    For example, create "secret.py" inside settings directory, this file should not be shared through
    a Version Control System like GIT or SVN.

    File format should look like:
        SERVER_EMAIL = 'server@email.com'
        SERVER_EMAIL_PASS = 'password'
        EMAIL_HOST_SMTP = 'smtp.gmail.com:587'
        CLIENT_EMAIL = 'client@email.com'

    For later use, do like:

    from django.conf import settings
    email = settings.SERVER_EMAIL
"""
from .secret import *