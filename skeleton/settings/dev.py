from .common import *

DEBUG = True
ALLOWED_HOSTS = []
SITE_URL = 'http://localhost:8000'


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/
STATIC_URL = '/static/'


# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'database',
        'USER': 'user',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': '3306',
    }
}

# Configs for RabbitMQ: Responsible for getting contact info (producer), queue and send email (consumer)
RABBIT_MQ_HOST = 'localhost'
RABBIT_MQ_PORT = 5672
RABBIT_MQ_USER = 'guest'
RABBIT_MQ_PASSWORD = 'guest'
BROKER_URL = 'amqp://guest:guest@localhost:5672//'
