import os

if os.environ["DJANGO_SETTINGS_MODULE"] == "skeleton.settings.prd":
    from .prd import *
else:
    from .dev import *
