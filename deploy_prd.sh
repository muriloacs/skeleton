#Kill current uwsgi instance
killall -9 uwsgi

#Set env variable to prd settings
export DJANGO_SETTINGS_MODULE=skeleton.settings.prd

#Start threads for rabbitMQ consumers
killall -9 celery
nohup celery -A skeleton worker -B -l INFO > /tmp/celery.log 2>&1 &

#Start server
nohup uwsgi --ini deploy.ini:prd > /dev/null 2>&1 &
